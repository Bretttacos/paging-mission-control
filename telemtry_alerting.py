import json
from datetime import datetime,timedelta

# constant of column names to create a dictionary with data
COLUMNS = ['timestamp','satellite-id','red-high-limit','yellow-high-limit','yellow-low-limit','red-low-limit','raw-value','component']
# global dictionary for use in multiple functions
violations = {}

def update_violations(satellite_id, component, timestamp):
    key = f'{satellite_id}-{component}' # create unique key for dict and allows later code to get id and component from it

    # Creating new key/value pair if it does not exist
    if key not in violations:
        violations[key] = [timestamp]
    else:
        violations[key].append(timestamp)

def convert_timestamp(timestamp_string):
    # convert string timestamp to datetime for time comparison
    format_string = "%Y%m%d %H:%M:%S.%f"
    datetime_object = datetime.strptime(timestamp_string, format_string)
    return datetime_object

def main():
    alerts = [] # list of alerts to be printed
    with open('file.txt', 'r') as my_file: # input file being loaded
        lines = my_file.readlines()

    for line in lines:
        line = line.replace('\n','').split('|')
        # split data into list and remove carriage return
        # then combine it with column names for more readability later
        data = dict(zip(COLUMNS, line))
        
        # converting string to float then comparing the raw value with the limit value
        # if fails limit test, adding to the violations dict
        if data['component'] == 'TSTAT' and float(data['raw-value']) > float(data['red-high-limit']):
            update_violations(data['satellite-id'], data['component'], data['timestamp'])

        if data['component'] == 'BATT' and float(data['raw-value']) < float(data['red-low-limit']):
            update_violations(data['satellite-id'], data['component'], data['timestamp'])
        
    for key, timestamps in violations.items():
        satellite_id, component = key.split('-')
        for first_violation in timestamps:
            violation_count = 0
            first_violation_datetime = convert_timestamp(first_violation)
            violation_period = first_violation_datetime + timedelta(minutes=5)
            # assigning the current timestamp in loop as first violation to create the interval
            # then comparing all timestamps to first violation and violation period of 5 minutes
            for timestamp in timestamps:
                if convert_timestamp(timestamp) >= first_violation_datetime and convert_timestamp(timestamp) < violation_period:
                    violation_count += 1

            # if the violation are greater than 2, remove those timestamps that fell within period
            # then assign a severity and add to the alert list for later printing
            if violation_count > 2:
                timestamps = timestamps[violation_count:]
                if component == 'TSTAT':
                    severity = 'RED HIGH'
                else:
                    severity = 'RED LOW'
                timestamp_str = first_violation_datetime.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + 'Z'
                alerts.append({'satelliteId': satellite_id, 'severity': severity, 'component': component, 'timestamp': timestamp_str})
            else:
                pass # if not enough violation in 5 minutes, go to next timestamp in the loop
            
    print(json.dumps(alerts, indent=4)) # print alerts

if __name__ == "__main__":
    main()
